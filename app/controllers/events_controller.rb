class EventsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  skip_before_filter :check_auth
  before_action :set_events, only: [:move, :edit, :update, :destroy]
  layout 'menu'
  def show
    if session[:user_id] != nil
      @user = User.find(session[:user_id])
    end
    @event = Event.find(params[:id])
  end

  def new
    @event = Event.new
    @timetable_id = params[:timetable_id]
    @date = params[:date]
    respond_to do |format|
      format.js
    end
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      respond_to do |format|
        format.js
      end
    end
  end

  def move
    @event.date = params[:date]
    @event.timetable_id = params[:timetable_id]
    @event.save
    respond_to do |format|
      format.js
    end
  end

  def edit
    @event  = Event.find(params[:id])
    @timetable_id = @event.timetable_id
    @date = @event.date
    respond_to do |format|
      format.js
    end
  end

  def update
    @event.update_attributes(event_params)
  end

  def destroy
    if @event.destroy
      respond_to do |format|
        format.js
      end
    end
  end

  def tasks
    respond_to do |format|
      format.js
    end
  end
  private

    def set_events
      @event = Event.find(params[:id])
    end
    def event_params
      params.require(:event).permit(:name, :date, :timetable_id, :only_time, :description, files: [])
    end
end
