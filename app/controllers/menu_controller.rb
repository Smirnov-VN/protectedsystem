#coding: utf-8
class MenuController < ApplicationController
  layout 'menu'
  skip_before_filter :check_auth

  def index
    @user = User.find(session[:user_id]) unless session[:user_id].nil?
  end



end
