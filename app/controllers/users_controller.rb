class UsersController < ApplicationController
  skip_before_filter :check_auth
  layout 'menu'
  # GET /users/new
  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to login_path
    end
  end

  def add
    user = User.find_by_login(params[:fio])
    group_users = Group.find(params[:id]).users
    unless user == nil or group_users.include? user
      group_users << user
    end
    redirect_to edit_group_path(id: params[:id])
  end

  private
  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :login, :pass)
  end
end
