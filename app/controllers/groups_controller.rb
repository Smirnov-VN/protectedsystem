#coding: utf-8
class GroupsController < ApplicationController
  helper :all
  layout 'menu'
  def index
    groups = @user.groups
    @groups = []
    groups.each do |group|
      @groups << {"number" => group.name,
                  "show_ref" => "<a href=\"#{group_path(group)}\">Просмотр</a>",
                  "edit_ref" => "<a href=\"#{edit_group_path(group)}\">Редактировать</a>"}
    end
  end

  def show
    @group = Group.find(params[:id])
    @users = []
    @group.users.each do |user|
      @users << {"fio" => "<a href=\"#{group_user_path(@group, user)}\">#{user.fio}</a>",
                    "login" => user.login}
    end
  end

  def new
  end

  def create
    group = Group.new(name: params[:number])
    group.create_timetable
    group.timetable.user_id = @user.id
    group.timetable.group_number = group.name
    group.timetable.save
    group.users << @user
    group.save
    redirect_to edit_group_path(group)
  end

  def edit
    @group = Group.find(params[:id])
    @users = []
    @group.users.each do |user|
      @users << {"fio" => user.fio,
                    "login" => user.login,
                    "delete" => ""}
    end
    @users << {"fio" =>'<input type="textbox" name="fio" placeholder="Логин" class="form-control"/>',
      "login" => '<input type="submit" value="Добавить" class="btn btn-primary">',
      "delete" => ""}
  end

  def update
    group = Group.find(params[:id])
    group.name = params[:number]
    group.save
    redirect_to groups_path(group)
  end

end
