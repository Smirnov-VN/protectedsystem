class ScheduleController < ApplicationController
  skip_before_filter :check_auth
  layout 'menu'

  def index
    if session[:user_id] == nil
      redirect_to root_url
    else
      @user = User.find(session[:user_id])
      @groups = @user.groups
      @timetables = @user.timetables
    end
  end

  def show
    if session[:user_id] != nil
      @user = User.find(session[:user_id])
    end
    @timetable = Timetable.find(params[:id])
    permit = @timetable.permit ? true : false
    if @user != nil
      permit = @timetable.user_id == @user.id ? true : permit
      @user.groups.each do |group|
        if @timetable.group != nil and group.id == @timetable.group.id
          permit = true
        end
      end
    end
    if permit
      @events = @timetable.events
      @date = params[:month] ? Date.parse(params[:month]) : Date.today
    else
      redirect_to root_url
    end
  end

end
