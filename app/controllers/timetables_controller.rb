class TimetablesController < ApplicationController

  skip_before_filter :verify_authenticity_token
  layout 'menu'

  def index
    if params[:month] != nil
      @date = Date.parse params[:month]
    else
      @date = Date.today
    end
    @user = User.find_by(id: session[:user_id])
    @event = Event.new
    @timetables = @user.timetables
    @timetables_showed = []
    show = session[:show]
    if show != nil
      @timetables_showed = Timetable.where(id: show.select{|k,v| v == '1'}.keys)
    end
    @template = TimetableTemplate.new
    @templates = TimetableTemplate.all
    if (session[:planning_task_id]!=nil)
      @task = PlanningTask.find(session[:planning_task_id])
    end
  end
  def init #if group haven't timetable => creates timetable
    @groups = Group.all
    @count = 0
    @groups.each do |group|
      if group.timetable==nil
        timetable = Timetable.new
        timetable.group_number = group.name
        group.timetable = timetable
        @count+= 1
      end
    end
    respond_to do |format|
      format.js
    end
  end
  def new
    @timetable = Timetable.new
  end

  def create
    @timetable = Timetable.new(timetable_params)
    @timetable.user_id = @user.id
    redirect_to :timetables if @timetable.save
  end
  def show #shows checked timetables
    if params[:month] != nil
      @date = Date.parse params[:month][:month]
    else
      @date = Date.today
    end
    show = params[:show]
    session[:show] = show
    @timetables = Timetable.where(id: show.select{|k,v| v == '1'}.keys)
    respond_to do |format|
      format.js
    end
  end
  def to_json #shows timetable events in to_json_form textarea
    @timetable = Timetable.find(params[:id])
    @events = @timetable.events
    @json = []
    @events.each do |event|
      @json << JSON[event.to_json]
    end
    respond_to do |format|
      format.js
    end
  end
  def paste #paste template json in from_json_form textarea
    @template = TimetableTemplate.find(params[:id])
    respond_to do |format|
      format.js
    end
  end
  def from_json #destroy all events of chosen timetable and creates new from json in from_json_form textarea
    if params[:month] != nil
      @date = Date.parse params[:month][:month]
    else
      @date = Date.today
    end
    @timetable = Timetable.find(params[:id])
    @timetable.events.each do |event|
      event.destroy
    end
    @timetable = Timetable.find(params[:id])
    if params[:json]!=''
      json = JSON.parse(params[:json])
      (0..json.length-1).each do |i|
        event = @timetable.events.new
        event.name =  json[i]['name']
        event.date = json[i]['date']
        if json[i]['only_time'] != nil
          event.only_time = Time.parse json[i]['only_time']
        end
        event.save
      end
    end
    respond_to do |format|
      format.js
    end
  end
  private
  def timetable_params
    params.require(:timetable).permit(:group_number, :permit)
  end
end
