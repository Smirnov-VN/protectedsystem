class Event < ActiveRecord::Base
  belongs_to :timetable
  belongs_to :user
  has_many :attachments, ->{ordering}, dependent: :destroy, inverse_of: :event
  attr_reader :files

  def self.create_by?(user, current_user)
    current_user && (user == current_user)
  end

  def edit_by?(u)
    self.class.create_by?(user, u)
  end

  def files=(val)
    val.each_with_index do |file, i|
      self.attachments.build(image: file, position: attachments.maximum(:position).to_i + i + 1)
    end
  end
end
