Rails.application.routes.draw do

  resources :attachments
  resources :users
  get "auth/login", to: "auth#login", as: "login"
  post "auth/authentificate"
  post "auth/logout", as: "logout"
  get  "auth/logout", as: "get_logout"

  resources :menu

  root 'menu#index'
  resources :groups
  post "groups/:id/add", to: "users#add", as: "add_to_group"
  get 'groups_execute' => 'groups#execute'
  get 'groups_commit'  => 'groups#commit', as: :groups_commit

  get "timetables", to: "timetables#index", as: "timetables"
  post "events/move"
  get "timetables/init"
  post "timetables/show"
  get "timetables/:id/to_json", to: "timetables#to_json", as: "to_json_timetable"
  get "timetables/paste"
  post "timetables/:id/from_json", to: "timetables#from_json", as: "from_json_timetable"
  get 'timetables_execute' => 'timetables#execute'
  get 'timetables_commit'  => 'timetables#commit', as: :timetables_commit
  resources :timetables
  resources :schedule
  resources :events
  resources :timetable_templates




  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
