# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171224173044) do

  create_table "attachments", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "position"
    t.belongs_to :event, index: true
    t.string   "comment"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.date     "date"
    t.integer  "timetable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
    t.datetime "only_time"
    t.belongs_to :user, index: true
    t.index ["timetable_id"], name: "index_events_on_timetable_id"
  end

  create_table "groups", force: :cascade do |t|
    t.text "name"
  end

  create_table "groups_users", force: :cascade do |t|
    t.belongs_to :user, index: true
    t.belongs_to :group, index: true
  end

  create_table "timetable_templates", force: :cascade do |t|
    t.string   "name"
    t.text     "json"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timetables", force: :cascade do |t|
    t.integer  "group_id"
    t.belongs_to :user, index: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "group_number"
    t.boolean  "permit"
    t.index ["group_id"], name: "index_timetables_on_group_id"
  end

  create_table "tokenlines", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "login"
    t.string   "pass"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.text    "fio"
    t.index ["user_id"], name: "index_users_on_user_id"
  end

end
